/*
Autor*innen: Deborah Schrag, Riaan Kämpfer
Programm für: Tastatur Eingabe auslesen, um das Mobile Roboterfahrzeug zu bewegen.

*/


#include "ros/ros.h"
#include "std_msgs/String.h"
#include <string>
#include <termios.h>
#include <unistd.h>
#include <sstream>

//Globale Variabeln
//Tank Motor Defines
#define UP 'w'
#define DOWN 's'
#define TURN_LEFT 'a'
#define TURN_RIGHT 'd'

#define SPEED_UP 'm'
#define SPEED_DOWN 'l'

#define STOP 'o'

//Grabber Defines
#define GRAB_OBJECT 'g'
#define CLOSE_GRABBER 'f'
#define OPEN_GRABBER 'h'
#define GRAB_FORWARD 't'
#define GRAB_BACKWARD 'b'

#define TILTDOWN_GRABBER 'z'
#define TILTUP_GRABBER 'v'



int main(int argc, char **argv)
{
	ros::init(argc, argv, "keyboard_node"); //keyboard_node ist der Node Name
	ros::NodeHandle nh;

	std::string keyboard_data;
	
	ros::Rate loop_rate(10); //Wie oft publishen wir 1x pro Sekunde
	
	ros::Publisher keyboard_node = nh.advertise<std_msgs::String>("keyboard_topic", 1000); // This is the name of the topic on which the publisher will publish messages. keyboard_node ist der name des Publishers, der unter dem Topic "keyboard_topic" Nachrichten/messages veröffentlicht/published. 
																						// Die Grösse der Reihe/Anzahl an messages, die der Publischer maximal zwischenspeichern/buffern kann, wird hier mit 1000 festgelegt. Weitere Nachrichten verdrängen ab dann die ältesten im Buffer.
	std_msgs::String keyboard_msg; //Message Objekt
	
	while (ros::ok)
	{
		char key;		
		//Termios
	    termios oldSettings, newSettings;
		tcgetattr(STDIN_FILENO, &oldSettings);  // Get current terminal settings

		newSettings = oldSettings;
		newSettings.c_lflag &= ~(ICANON | ECHO);  // Disable canonical mode and echoing
		tcsetattr(STDIN_FILENO, TCSANOW, &newSettings);  // Apply new terminal settings

		std::cin.read(&key, 1);  // Read a single character without waiting for Enter (keypress wird gleichzeitig als Enter-Event interpretiert)
		tcsetattr(STDIN_FILENO, TCSANOW, &oldSettings);  // Restore original terminal settings
		//Termios
			
		switch (key)
		{
			case UP:
			keyboard_data = "UP";
			break;
			
			case TURN_LEFT:
			keyboard_data = "TURN_LEFT";
			break;
			
			case DOWN:
			keyboard_data = "DOWN";
			break;
			
			case TURN_RIGHT:
			keyboard_data = "TURN_RIGHT";
			break;
			
			case SPEED_UP:
			keyboard_data = "SPEED_UP";
			break;
			
			case SPEED_DOWN:
			keyboard_data = "SPEED_DOWN";
			break;
			
			case STOP:
			keyboard_data = "STOP";
			break;
			
			case GRAB_OBJECT:
			keyboard_data = "GRAB_OBJECT";
			break;
			
			case OPEN_GRABBER:
			keyboard_data = "OPEN_GRABBER";
			break;
			
			case CLOSE_GRABBER:
			keyboard_data = "CLOSE_GRABBER";
			break;
			
			case GRAB_FORWARD:
			keyboard_data = "GRAB_FORWARD";
			break;
			
			case GRAB_BACKWARD:
			keyboard_data = "GRAB_BACKWARD";
			break;
			
			case TILTDOWN_GRABBER:
			keyboard_data = "TILTDOWN_GRABBER";
			break;
			
			case TILTUP_GRABBER:
			keyboard_data = "TILTUP_GRABBER";
			break;
		}
		
		

		//ROS Daten Publishen
		keyboard_msg.data = keyboard_data; //Was ist in unserem keyboard_msg Objekt
		keyboard_node.publish(keyboard_msg); //published keyboard_msg zum Topic keyboard_topic
		
		ros::spinOnce(); //Checks for callback
		
			//loop_rate.sleep(); // Setzt eine entspr. Pause in den loop ein, 
								// damit die Looprate nicht mit Maximalgeschw. sondern 
								// mit der gesetzten Rate durchgeführt wird.
		}

	return 0;
}




