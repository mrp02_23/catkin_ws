//Move Node
/*Bewegungsabläufe werden hier definiert.
Via Subscriber erhalten wir daten aus unserer Umgebung und verarbeiten diese.
Via Publisher werden unsere aktuellen Geschwindigkeiten ausgegeben und der Bewegungsstatus*/

//TODO Subscriber IMU + LIDAR POSITION -> angle
//If pigpio doesnt initialise:
// cat /var/run/pigpio.pid
//sudo kill -9 NUMBERFROMABOVE



#include "ros/ros.h"
#include <string>
#include <iostream>
#include <pigpio.h>
#include "std_msgs/String.h"


#define  offset_left 10;
#define  offset_right 10;

//Pin Deklaration Motoren
//Links
#define motorPinA1 27
#define motorPinA2 22
    
#define motorPinB1 23
#define motorPinB2 24

#define pwmPin_LV 5
#define pwmPin_LH 6

//Rechts
#define motorPinA1_2 16
#define motorPinA2_2 19
    
#define motorPinB1_2 20
#define motorPinB2_2 26

#define pwmPin_RV 12
#define pwmPin_RH 13


//PWM Setup
#define motor_freq 10000    


//General Variabels
int speed_left = 100;
int speed_right = 100;
int speed = 200;
float angle = 0;
const char* order;
std::string order_string;

//Funktionsdeklarationen
void motor_init();
void forward(int);
void backwards(int); //Unser Fahrzeug bewegt sich rückwärts
void turn_onplace_right(int); //Fahrzeug dreht sich auf der Stelle cw
void turn_onplace_left(int); //Fahrzeug dreht sich auf der Stelle CCW
void stop();

void writeMsgToLog_keyboard(const std_msgs::String::ConstPtr &keyboard_msg)
    {
        ROS_INFO("I heard: [%s]", keyboard_msg->data.c_str());
        order = keyboard_msg->data.c_str();
        order_string = order;
    }

int main(int argc, char **argv)
{
    ros::init(argc, argv, "move"); //Der Name unserer Node
    ros::NodeHandle nh; //starts the nod
    ros::Subscriber keyboard_sub = nh.subscribe("keyboard_topic", 1000, writeMsgToLog_keyboard);   
        
    ROS_INFO_STREAM("MOTOR STARTET");
    motor_init();
    
    while(ros::ok())
    {
    int count = 0;
    
        if (order_string == "UP")
        {
            if (count == 0)
            {
                ROS_INFO_STREAM("DRIVING FORWARDS");
                count++;
            }
            forward(speed);
        } 
        else if (order_string == "TURN_LEFT")
        {
            ROS_INFO_STREAM("TURNING LEFT");
            turn_onplace_left(speed);
        } 
        else if (order_string == "DOWN")
        {
            ROS_INFO_STREAM("DRIVING BACKWARD");
            backwards(speed);
        } 
        else if (order_string == "TURN_RIGHT")
        {
            ROS_INFO_STREAM("TURNING RIGHT");
            turn_onplace_right(speed);
        } 
        else if (order_string == "STOP")
        {
            //ROS_INFO_STREAM("STOPPING");
            stop();
        } 
        
        if (order_string == "SPEED_UP")
        {
            speed += 50;
            if(speed > 1000)
            {
                speed = 1000;
            }
            
            ROS_INFO_STREAM("SPEED UP");
            gpioPWM(pwmPin_LV, speed);
            gpioPWM(pwmPin_LH, speed);
            gpioPWM(pwmPin_RV, speed);
            gpioPWM(pwmPin_RH, speed);
        } 
        else if (order_string == "SPEED_DOWN")
        {
            speed -= 50;
            if (speed < 0) 
            {
                speed = 0;
            }
            
            ROS_INFO_STREAM("SPEED DOWN");
            gpioPWM(pwmPin_LV, speed);
            gpioPWM(pwmPin_LH, speed);
            gpioPWM(pwmPin_RV, speed);
            gpioPWM(pwmPin_RH, speed);
            
        } 
       
        else 
        {
        }
        
        
              
        //Error Handling
        if(!ros::ok())
        {
            gpioTerminate();
            std::cout << "ROS NOT OKAY, Reset Pi Pins" << std::endl;
        }
        
        ros::spinOnce();
        ros::Duration(0.1).sleep();

    }


        
  
    
    return 0;
}


//Funktionen
void motor_init()
{    
    gpioInitialise(); //pigpio initialisiere
    ROS_INFO_STREAM("MOTOR INITIALISIERT");
    gpioSetMode(pwmPin_LV, PI_OUTPUT);
    gpioSetMode(pwmPin_LH, PI_OUTPUT);
    gpioSetMode(pwmPin_RV, PI_OUTPUT);
    gpioSetMode(pwmPin_RH, PI_OUTPUT);

    gpioSetMode(motorPinA1, PI_OUTPUT);
    gpioSetMode(motorPinA2, PI_OUTPUT);
    gpioSetMode(motorPinB1, PI_OUTPUT);
    gpioSetMode(motorPinB2, PI_OUTPUT);

    gpioSetMode(motorPinA1_2, PI_OUTPUT);
    gpioSetMode(motorPinA2_2, PI_OUTPUT);
    gpioSetMode(motorPinB1_2, PI_OUTPUT);
    gpioSetMode(motorPinB2_2, PI_OUTPUT);

    gpioSetPWMfrequency(pwmPin_LV, motor_freq);
    gpioSetPWMfrequency(pwmPin_LH, motor_freq);
    gpioSetPWMfrequency(pwmPin_RV, motor_freq);
    gpioSetPWMfrequency(pwmPin_RH, motor_freq);

    gpioSetPWMrange(pwmPin_LV, 1000);
    gpioSetPWMrange(pwmPin_LH, 1000);
    gpioSetPWMrange(pwmPin_RV, 1000);
    gpioSetPWMrange(pwmPin_RH, 1000);

    gpioPWM(pwmPin_LV, speed_left);
    gpioPWM(pwmPin_LH, speed_left);
    gpioPWM(pwmPin_RV, speed_right);
    gpioPWM(pwmPin_RH, speed_right);
}


void forward(int speed) //Unser Fahrzeug bewegt sich vorwärts
{    
    //ROS_INFO_STREAM("FAHRE VORWÄRTS");
    gpioWrite(motorPinA1, 1);
    gpioWrite(motorPinA2, 0);
    gpioWrite(motorPinB1, 1);
    gpioWrite(motorPinB2, 0);

    gpioWrite(motorPinA1_2, 1);
    gpioWrite(motorPinA2_2, 0);
    gpioWrite(motorPinB1_2, 1);
    gpioWrite(motorPinB2_2, 0);

    gpioPWM(pwmPin_LV, speed);
    gpioPWM(pwmPin_LH, speed);
    gpioPWM(pwmPin_RV, speed);
    gpioPWM(pwmPin_RH, speed);
} 

void backwards(int speed) //Unser Fahrzeug bewegt sich rückwärts
{    
    //ROS_INFO_STREAM("FAHRE RÜCKWÄRTS");
    gpioWrite(motorPinA1, 0);
    gpioWrite(motorPinA2, 1);
    gpioWrite(motorPinB1, 0);
    gpioWrite(motorPinB2, 1);
    
    gpioWrite(motorPinA1_2, 0);
    gpioWrite(motorPinA2_2, 1);
    gpioWrite(motorPinB1_2, 0);
    gpioWrite(motorPinB2_2, 1);
    
    gpioPWM(pwmPin_LV, speed);
    gpioPWM(pwmPin_LH, speed);
    gpioPWM(pwmPin_RV, speed);
    gpioPWM(pwmPin_RH, speed);
} 

void turn_onplace_right(int speed) //Fahrzeug dreht sich auf der Stelle cw
{
    //ROS_INFO_STREAM("DREHE RECHTS");
    gpioPWM(pwmPin_LV, speed);
    gpioPWM(pwmPin_LH, speed);
    gpioPWM(pwmPin_RV, speed);
    gpioPWM(pwmPin_RH, speed);

    
    gpioWrite(motorPinA1, 1);
    gpioWrite(motorPinA2, 0);
    gpioWrite(motorPinB1, 1);
    gpioWrite(motorPinB2, 0);
    
    gpioWrite(motorPinA1_2, 0);
    gpioWrite(motorPinA2_2, 1);
    gpioWrite(motorPinB1_2, 0);
    gpioWrite(motorPinB2_2, 1);
} 

void turn_onplace_left(int speed) //Fahrzeug dreht sich auf der Stelle CCW
{
    //ROS_INFO_STREAM("DREHE LINKS");
    gpioPWM(pwmPin_LV, speed);
    gpioPWM(pwmPin_LH, speed);
    gpioPWM(pwmPin_RV, speed);
    gpioPWM(pwmPin_RH, speed);


    gpioWrite(motorPinA1, 0);
    gpioWrite(motorPinA2, 1);
    gpioWrite(motorPinB1, 0);
    gpioWrite(motorPinB2, 1);
    
    gpioWrite(motorPinA1_2, 1);
    gpioWrite(motorPinA2_2, 0);
    gpioWrite(motorPinB1_2, 1);
    gpioWrite(motorPinB2_2, 0);
} 

void stop()
{
    //ROS_INFO_STREAM("STOPPE");
    gpioWrite(motorPinA1, 0);
    gpioWrite(motorPinA2, 0);
    gpioWrite(motorPinB1, 0);
    gpioWrite(motorPinB2, 0);
    
    gpioWrite(motorPinA1_2, 0);
    gpioWrite(motorPinA2_2, 0);
    gpioWrite(motorPinB1_2, 0);
    gpioWrite(motorPinB2_2, 0);
    //gpioTerminate();
    //order_string = "";
} 
