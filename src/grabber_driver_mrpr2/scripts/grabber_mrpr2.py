#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Created on Fri May 26 06:50:01 2023
@author: Deborah & Riaan

Python Program for using our 2 Servos on our TANK.
Programm Subscribes to topic /keyboard_topic and reads keyboard inputs.
The Grabber can close and open to grab objects. The linear Actuator can move the Grabber Forward and Backwards.

TODO:
Publisher
Subscriber /keyboard_topic, /object_recognition
"""
# Python initialization
from adafruit_servokit import ServoKit
import rospy
from std_msgs.msg import String
import numpy as np
import time

#Node

# Global variables
    # PWM-Board-Channel-Zuweisungen
Cam_Servo = 0
Act_Servo = 1
Tilt_Servo = 2
    # leere Stringvariable für Befehlsstrings anlegen
order_string = ""

# Number of Channels on our Multi-PWM-Board
kit = ServoKit(channels=16)

#default angle for grabber arm
phi_0 = 100


# ROS
# Callback function

def callback(keyboard_msg):
    rospy.loginfo(rospy.get_caller_id() + " I heard %s", keyboard_msg.data)
    global order_string
    order_string = keyboard_msg.data
    manual_grabbing(order_string)


#def callback(MESSAGEOBJECT shape_area.int???):
 #   rospy.loginfo(rospy.get_caller_id() + " I heard %s", keyboard_msg.data)

# Subscriber
def subscriber():
    rospy.init_node('servo_arm_node')
    rospy.Subscriber('/keyboard_topic', String, callback)
    #rospy.Subscriber("/is_Cube", int, callback_Area)
    rospy.spin()
    

# Grabber arm Winkel initialisieren (Arm waagrecht stellen):
# kit.servo[Tilt_Servo].angle = 90
#target_angle = 90
#time_interval = 0.27 # Geschwindigkeit der Kippbewegung anpassen (grösserer Wert==> langsamer ...)
#current_angle = kit.servo[Tilt_Servo].angle
#
#angles = np.linspace(current_angle, target_angle, num=abs(target_angle - current_angle) + 1)
#for angle in angles:
#    kit.servo[Tilt_Servo].angle = angle
#    time.sleep(time_interval)

# Manual Grabbing

def close_grabber():
    for i in range(90, 0, -1): # FOR-Loop welcher in einer Spanne von 90 bis 1 iteriert (Wert 0 ist kein weiterer Iter.schritt)  mit einer Schrittweite von -1
            kit.servo[Cam_Servo].angle = i
            if kit.servo[Cam_Servo].angle <= 0:
                kit.servo[Cam_Servo].angle = 0
                
def open_grabber():
    for i in range(0, 90):
            kit.servo[Cam_Servo].angle = i
            if kit.servo[Cam_Servo].angle >= 90:
                kit.servo[Cam_Servo].angle = 90
                
def grab_forward():
      for i in range(0, 145):
            kit.servo[Act_Servo].angle = i
            if kit.servo[Act_Servo].angle >= 145:
                kit.servo[Act_Servo].angle = 145
                
def grab_backward():
    for i in range(145, 0, -1):
        kit.servo[Act_Servo].angle = i
        if kit.servo[Act_Servo].angle <= 0:
            kit.servo[Act_Servo].angle = 0

def stop():
        kit.servo[Tilt_Servo].angle = 100
        kit.servo[Cam_Servo].angle = 90
        kit.servo[Act_Servo].angle = 0

def tilt_down():
    h=160; # Höhe des Greifarmkipplagers
    l=330; # (Max.) Länge des Greifarmes (Absolut) ab Kipplagerpunkt (l könnte als Funktion der obigen aktuellen Servoeinstellungen deklariert werden!)
    time_interval = 0.10
    phi=phi_0-(np.arctan(h/l)*180/np.pi); # Arm-Kippwinkel (Für lmax--> phi_min=arctan(h/l_max)*180/pi=21.5°)
    phi=int(phi) # phi in eine ganze Zahl umwandeln für den for-loop
    for i in range(phi_0, phi, -1):
        kit.servo[Tilt_Servo].angle = i
        time.sleep(time_interval)
        if kit.servo[Tilt_Servo].angle > phi:
            kit.servo[Tilt_Servo].angle = phi
    
def tilt_up():
    #h=130; # Höhe des Greifarmkipplagers
    #l=330; # (Max.) Länge des Greifarmes (Absolut) ab Kipplagerpunkt (l könnte als Funktion der obigen aktuellen Servoeinstellungen deklariert werden!)
    #phi=90-(np.arctan(h/l)*180/np.pi); # Arm-Kippwinkel (Für lmax--> phi_min=arctan(h/l_max)*180/pi=21.5°)
    #phi=int(phi) # phi in eine ganze Zahl umwandeln für den for-loop
    #for i in range(phi, 0):
    #    kit.servo[Tilt_Servo].angle = i
    #    if kit.servo[Tilt_Servo].angle <= phi-3:
    #kit.servo[Tilt_Servo].angle = 90
    target_angle = phi_0 
    time_interval = 0.10 # Geschwindigkeit der Kippbewegung anpassen (grösserer Wert==> langsamer ...)
    current_angle = kit.servo[Tilt_Servo].angle

    angles = np.linspace(current_angle, target_angle, num=abs(target_angle - current_angle) + 1)
    for angle in angles:
        kit.servo[Tilt_Servo].angle = angle
        time.sleep(time_interval)


    
def manual_grabbing(order_string):
    if order_string == "CLOSE_GRABBER":
        close_grabber()
            
    elif order_string == "OPEN_GRABBER":
        open_grabber()
        
    elif order_string == "GRAB_BACKWARD":
        grab_backward()
      
    elif order_string == "GRAB_FORWARD":
        grab_forward()
        
    elif order_string == "TILTDOWN_GRABBER":
        tilt_down()
        
    elif order_string == "TILTUP_GRABBER":
        tilt_up()
        
    elif order_string == "STOP":
        stop()
        
    elif order_string == "GRAB_OBJECT":
        open_grabber()
        grab_forward()
        tilt_down()
        close_grabber()
        tilt_up()
        grab_backward()
                
if __name__ == '__main__':
    subscriber() 

