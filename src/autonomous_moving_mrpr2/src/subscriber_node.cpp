#include "ros/ros.h"
#include "std_msgs/String.h"
#include "autonomous_moving_mrpr2/person_data.h" //Muss als h file anstelle von msg sein!
#include "object_recognition_mrpr2/recognition_data.h" //Inkludieren wegen benötigten Angaben => object_recognition_mrpr2
#include "wiringPi.h"




//Datenausgabe 
void writeMsgToLog(const autonomous_moving_mrpr2::person_data &person_data)
{
    ROS_INFO("Name: %s", person_data.name.c_str());
    ROS_INFO("Age: %i", person_data.age);
    ROS_INFO("Color: %s", person_data.color.c_str());
}

void writeMsgTo_object_recognition_log(const object_recognition_mrpr2::recognition_data &recognition_data)
{
    ROS_INFO("%s", recognition_data.is_cube.c_str());
    ROS_INFO("%s", recognition_data.not_cube.c_str());
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "Subscriber");
    ros::NodeHandle nh; //starts the node

    ros::Subscriber topic_sub = nh.subscribe("tutorial", 1000, writeMsgToLog); //tutorial ist der Topic name, writeMsgToLog ist unsere Callbackfunction

    ros::Subscriber object_recognition_sub = nh.subscribe("IsCube", 1000, writeMsgTo_object_recognition_log); //Wir subscriben zum topic IsCube
    //IsCube ist unser topic aus recognition_publisher.cpp
    //writeMsgTo_object_recognition_log ist unsere Callbackfunction


    wiringPiSetup();
    //ros::init(argc, argv, "Track_Speed_Publisher")
    //ros::Publisher left_track_speed_pub = nh.advertise<autonomous_moving_mrpr2::person_data>("left_track_speed", 1000)
    
    ROS_INFO("I AM HERRE HELP ME");

    // PINOUT IM TERMINAL MIT: gpio readall
    int left_track_speed = 20;
    int motorPinA1 = 25; // GPIO Pin 23 //physisch 33
    int motorPinA2 = 24; // GPIO Pin 24
    int pwmPinA = 23;    // GPIO Pin 12 //physisch 37
    
    int motorPinB1 = 22; // GPIO Pin 20
    int motorPinB2 = 21; // GPIO Pin 21

    pinMode(pwmPinA, PWM_OUTPUT);
    pwmSetClock(24*100000);
    pwmSetRange(2000);
    pwmWrite(pwmPinA, 1000); //Dutycycle ist auf 50% 1024 ist max


    //Motor Links Vorne
    pinMode(motorPinB1, OUTPUT);
    pinMode(motorPinB2, OUTPUT);
    //pinMode(pwmPinA, PWM_OUT); //Das Pwm Signal auf einer Seite ist jeweils gleich 

    digitalWrite(motorPinB1, HIGH);
    digitalWrite(motorPinB2, LOW);

    //Motor Linkes Hinten
    pinMode(motorPinA1, OUTPUT);
    pinMode(motorPinA2, OUTPUT);

    digitalWrite(motorPinA1, HIGH);
    digitalWrite(motorPinA2, LOW);

    //pwmWrite(pwmPin, 512); //Dutycycle ist auf 50% 1024 ist max

    ros::spin();
    return 0;
}
