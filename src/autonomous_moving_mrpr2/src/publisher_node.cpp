#include <ros/ros.h>
#include "std_msgs/String.h"
#include "autonomous_moving_mrpr2/person_data.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Publisher"); //Publisher ist der Node Name
    ros::NodeHandle nh; //Wir starten unsere Node

    //ros::Publisher topic_pub = nh.advertise<std_msgs::String>("tutorial", 1000);
    ros::Publisher topic_pub = nh.advertise<autonomous_moving_mrpr2::person_data>("tutorial", 1000); //Turial ist unser Topic

    ros::Rate loop_rate(1); //Wie oft publishen wir 1x pro Sekunde

    while(ros::ok())
    {
        autonomous_moving_mrpr2::person_data person_data;
        person_data.name = "Deborah Schraggg";
        person_data.age = 24;
        person_data.color = "yellow eeend violettt";

        //std_msgs::String msg; //Message Objekt

        //msg.data = "Hello World!";

        //topic_pub.publish(msg); //published msg zum Topic tutorial

        topic_pub.publish(person_data); //published person_data zum Topic tutorial

        ros::spinOnce(); //Checks for callback
        loop_rate.sleep();
    }

    return 0;
}
