#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/photo.hpp>




#define LOW_THRESHOLD 50 
#define RATIO 3

#define CUBEDET_MIN_LINES 4


class shapeDetection
{
  public:
    typedef struct 
    {
      uint16_t min_H;
      uint16_t min_S;
      uint16_t min_V;
      uint16_t max_H;
      uint16_t max_S;
      uint16_t max_V;
    }colors_hsv_t;
    

  public:
    shapeDetection(cv::Mat img_in);
    shapeDetection(void);
    ~shapeDetection();
    
    void newFrame(cv::Mat img_in);
    cv::Mat createMask(colors_hsv_t color);
    cv::Rect cubeDetection();
    cv::Vec3i circleDetection();

    cv::Mat showLines(void);
    cv::Mat showCircles(void);

    cv::Mat captureImage(cv::Mat img_in, std::string windowName);

  public:
    bool cubeDetected = 0;
    bool ballDetected = 0;

  private:
    void blur(cv::Mat img_in, uint16_t kernel_size);
    void findLines(cv::Mat img_in);
    void groupLines();
    void findCircles(cv::Mat img_in);

    

    cv::Mat imgOrig;
    cv::Mat mask;
    cv::Mat imgMasked;
    cv::Mat imgGray;
    cv::Mat imgHSV;
    cv::Mat imgEdge;
    cv::Mat imgLines;
    cv::Mat imgCircles;

    std::vector<cv::Vec4i> lines;
    std::vector<cv::Vec3f> circles;
};

extern const shapeDetection::colors_hsv_t LIGHT_BLUE; 
extern const shapeDetection::colors_hsv_t ORANGE;
extern const shapeDetection::colors_hsv_t VIOLET;
