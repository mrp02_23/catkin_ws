//-----------------------------------------------
// project:     Image processing 1 project   
// file:    	main.cpp
// school:      FHGR
// author:      Lukas Marti
// date:        09.04.2023
// version:     1.0
// toolchain:	Editor:     vim
//		        Compiler:	gcc
//		        Builder:	Cmake
//		        OS:		    Linux (Pop_OS 22.04)
//
// brief:       Testing the shape detection on a        
//              linux device, to later port to 
//              Raspberry Pi
//------------------------------------------------

#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <ros/ros.h>
#include <object_recognition_mrpr2/shapeDetection.hpp>

//-----------------------------
//Defines
//----------------------------
#define FPS 10

//-----------------------------
//Constants
//----------------------------

//-----------------------------
//Function declarations
//----------------------------

//-----------------------------
//Main 
//----------------------------
int main(int argc, char* argv[])
{
    ros::init(argc, argv, "shape_node"); //shape_node ist der Node Name
    ros::NodeHandle nh;
    
    
    std::string windowName = "Input Image";

    cv::VideoCapture capture; 
    capture.open(0);
    
    shapeDetection detector;
    cv::Mat frame;
    char c;

    capture >> frame;
    cv::namedWindow(windowName, cv::WINDOW_AUTOSIZE);
    cv::imshow(windowName, frame);

    
    while(1)
    {
        capture >> frame;

        if(c == 'p') // Wenn die Taste "p" gedrückt wurde
        {
            detector.captureImage(frame, windowName);
        }   

        //BEGINN
        if (c == 27) // Wenn die ESC-Taste gedrückt wurde
        {
            break; // Aus der Schleife ausbrechen
        }
        //END

        c = cv::waitKey(1000. / FPS);
        
    }

    



    return(0);
}


