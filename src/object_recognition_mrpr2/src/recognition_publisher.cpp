#include "ros/ros.h"
#include "object_recognition_mrpr2/recognition_data.h" 
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>





int main(int argc, char **argv)
{
    //ROS PUBLISHER
    ros::init(argc, argv, "recognition_publisher_node"); //Der Name unserer Node
    ros::NodeHandle nh; //Wir starten unsere Node

    ros::Publisher recognition_publisher = nh.advertise<object_recognition_mrpr2::recognition_data>("IsCube", 1000); //Der Topic Name ist "IsCube"

    ros::Rate loop_rate(1); //Wir Publishen 1x pro Sekunde


    while(ros::ok())
    {
        object_recognition_mrpr2::recognition_data recognition_data;
        recognition_data.is_cube = "Es ist ein Wuerfel";
        recognition_data.not_cube = "Es ist kein Wuerfel";

        recognition_publisher.publish(recognition_data); //Wir Publishen unsere Daten
        
        ros::spinOnce();
        loop_rate.sleep();
    }
    //ROS PUBLISHER END

    return 0;
}
