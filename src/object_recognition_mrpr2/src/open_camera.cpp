#include "ros/ros.h"
#include "std_msgs/String.h"
#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


int main(int argc, char **argv)
{

    ros::init(argc, argv, "open_camera_node"); //Der Name unserer Node
    ros::NodeHandle nh; //Wir starten unsere Node

    cv::VideoCapture cap(0);
    cap.set(cv::CAP_PROP_FRAME_WIDTH, 320);
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, 240);

    if(!cap.isOpened())
   {
        std::cout << "Can't access the camera";
   } 
   else
  {
        while(true)
       {
            cv::Mat frame;
            cap >> frame;
            if(frame.empty())
           {
                std::cout << "NO DATA FROM WEBCAM";
                break;
           } 
           cv::imshow("Frame|Press 'q' to quit", frame);
           if(cv::waitKey(30) == 'q')
          {
                break;
          } 
       } 
  } 

  return 0;
}
